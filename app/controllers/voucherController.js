// Import thư viện Mongoose
const mongoose = require("mongoose");

// Import Module Voucher Model
const voucherModel = require("../models/voucherModel");

const getAllVoucher = (request, response) => {
    // B1: Chuẩn bị dữ liệu
    // B2: Validate dữ liệu
    // B3: Gọi Model tạo dữ liệu
    voucherModel.find((error, data) => {
        if (error) {
            return response.status(500).json({
                status: "Internal server error",
                message: error.message
            })
        }

        return response.status(200).json({
            status: "Get all Voucher successfully",
            data: data
        })
    })
}

const createVoucher = (request, response) => {
    // B1: Chuẩn bị dữ liệu
    const body = request.body;
    // voucherCode: String, unique
    // pizzaSize: String, required
    // pizzaType: String, required
    // voucher: ObjectID, ref: Voucher
    // drink: ObjectID, ref: Drink
    // status: String, required


    // B2: Validate dữ liệu
    // Kiểm tra voucherCode có hợp lệ hay không
    if (!body.voucherCode) {
        return response.status(400).json({
            status: "Bad Request",
            message: "voucherCode không hợp lệ"
        })
    }

    // B3: Gọi Model tạo dữ liệu
    const newVoucher = {
        _id: mongoose.Types.ObjectId(),
        voucherCode: body.voucherCode,
        phanTramGiamGia: body.phanTramGiamGia
    }

    voucherModel.create(newVoucher, (error, data) => {
        if (error) {
            return response.status(500).json({
                status: "Internal server error",
                message: error.message
            })
        }

        return response.status(201).json({
            status: "Create Voucher successfully",
            data: data
        })
    })
}

const getVoucherById = (request, response) => {
    // B1: Chuẩn bị dữ liệu
    const voucherId = request.params.voucherId;

    // B2: Validate dữ liệu
    if (!mongoose.Types.ObjectId.isValid(voucherId)) {
        return response.status(400).json({
            status: "Bad Request",
            message: "voucherID không hợp lệ"
        })
    }

    // B3: Gọi Model tạo dữ liệu
    voucherModel.findById(voucherId, (error, data) => {
        if (error) {
            return response.status(500).json({
                status: "Internal server error",
                message: error.message
            })
        }

        return response.status(200).json({
            status: "Get detail Voucher successfully",
            data: data
        })
    })
}

const updateVoucherById = (request, response) => {
    // B1: Chuẩn bị dữ liệu
    const voucherId = request.params.voucherId;
    const body = request.body;

    // B2: Validate dữ liệu
    if (!mongoose.Types.ObjectId.isValid(voucherId)) {
        return response.status(400).json({
            status: "Bad Request",
            message: "voucherID không hợp lệ"
        })
    }

    if (body.voucherCode !== undefined && body.voucherCode.trim() === "") {
        return response.status(400).json({
            status: "Bad Request",
            message: "voucherCode không hợp lệ"
        })
    }


    if (body.phanTramGiamGia !== undefined && (isNaN(body.phanTramGiamGia) || body.phanTramGiamGia < 0)) {
        return response.status(400).json({
            status: "Bad Request",
            message: "phanTramGiamGia không hợp lệ"
        })
    }

    // B3: Gọi Model tạo dữ liệu
    const updateVoucher = {}

    if (body.voucherCode !== undefined) {
        updateVoucher.voucherCode = body.voucherCode
    }

    if (body.phanTramGiamGia !== undefined) {
        updateVoucher.phanTramGiamGia = body.phanTramGiamGia
    }

    voucherModel.findByIdAndUpdate(voucherId, updateVoucher, (error, data) => {
        if (error) {
            return response.status(500).json({
                status: "Internal server error",
                message: error.message
            })
        }

        return response.status(200).json({
            status: "Update Voucher successfully",
            data: data
        })
    })
}

const deleteVoucherById = (request, response) => {
    // B1: Chuẩn bị dữ liệu
    const voucherId = request.params.voucherId;

    // B2: Validate dữ liệu
    if (!mongoose.Types.ObjectId.isValid(voucherId)) {
        return response.status(400).json({
            status: "Bad Request",
            message: "voucherID không hợp lệ"
        })
    }

    // B3: Gọi Model tạo dữ liệu
    voucherModel.findByIdAndDelete(voucherId, (error, data) => {
        if (error) {
            return response.status(500).json({
                status: "Internal server error",
                message: error.message
            })
        }

        return response.status(200).json({
            status: "Delete Voucher successfully"
        })
    })
}

module.exports = {
    getAllVoucher,
    createVoucher,
    getVoucherById,
    updateVoucherById,
    deleteVoucherById
}